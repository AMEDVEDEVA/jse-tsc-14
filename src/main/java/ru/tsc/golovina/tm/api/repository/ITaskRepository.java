package ru.tsc.golovina.tm.api.repository;

import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    public List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllTaskByProjectId(String projectId);

    boolean existsById(String id);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskById(String taskId);

    void removeAllTaskByProjectId(String projectId);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

}
