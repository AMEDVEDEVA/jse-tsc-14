package ru.tsc.golovina.tm.controller;

import ru.tsc.golovina.tm.api.controller.IProjectController;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.api.service.IProjectTaskService;
import ru.tsc.golovina.tm.enumerated.Sort;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjects() {
        List<Project> projects;
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        if (sort != null || !sort.isEmpty()) {
            Sort sortType = Sort.valueOf(sort);
            projects = projectService.findAll(sortType.getComparator());
        } else projects = projectService.findAll();
        if (projects.size() <= 0) {
            System.out.println("Project list is empty now");
            return;
        }
        System.out.println("[LIST PROJECTS]");
        for (Project project : projects)
            showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProject(Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        showProject(project);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id)) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void startByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void startByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void finishById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void finishByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void finishByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void changeStatusById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void changeStatusByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null) System.out.println("Incorrect values");
    }

}
